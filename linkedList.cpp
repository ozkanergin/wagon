#include <iostream>
#include <stdio.h>
#include <string.h>

#include "linkedList.h"

#define MATERIAL_CAPACITY 2000

using namespace std;

void Train::create() {
    head = NULL;
};
/********************************************************************************************************************************************/
bool Train::IsMaterialExists(char material, materialNode* materialnode)		// Check the wagoon has material or not.
{
	bool result = false;
    while (materialnode!= NULL)
    {
		if (materialnode->id == material) {
			result = true;
			break;
		}
        materialnode = materialnode->next;
    }
	return result;
}
/********************************************************************************************************************************************/
materialNode* Train::GetMaterialNode(char material,materialNode * head)		// If it returns null , wagon does not include this material.
{
    materialNode* node_pointer = head;

    if (node_pointer != NULL)
    {
        while (node_pointer != NULL)
        {
            if (node_pointer->id == material)
            {
                break;
            }
			node_pointer = node_pointer->next;
        }        
    }
    return node_pointer;
}
/********************************************************************************************************************************************/
materialNode* Train::GetMaterialNodeUnloading(char material,materialNode * head)		// If it returns null , wagon does not include this material.
{
    materialNode* node_pointer = head;

    if (node_pointer != NULL)
    {
        while (node_pointer != NULL)
        {
            if (node_pointer->id == material && node_pointer->weight != 0)
            {
                break;
            }
			node_pointer = node_pointer->next;
        }        
    }
    return node_pointer;
}
/********************************************************************************************************************************************/

wagonNode* Train::GetNextAvailableWagon(char material, wagonNode* starting_node)
{
    wagonNode* current = starting_node;
    if (starting_node != NULL)
    {
        while (current != NULL)
        {
            materialNode* mnode = this->GetMaterialNode(material, current->material);		
            if (NULL == mnode)		// Check the wagoon. If node does not exist,  Return the wagoon.
            {
                return current;
            }
            else
            {
                if (mnode->weight < MATERIAL_CAPACITY)
                {
                    return current;
                }
            }

			// If there is no wagoon anymore , return null.
            if (current->next == NULL)
            {
                current    = NULL;
            }
            else
            { 
                current = current->next;
            }
        }
        return current;
    }

}

void Train::addMaterial(char material, int weight) {
    //FILL THIS FUNCTION ACCORDINGLY
    if (head == NULL)
    {
        head           = new wagonNode();
        head->wagonId  = 1;
        head->next     = NULL;
		//head->material = new materialNode();
    }


	// If weigth is not completed , recursively call this functions.
	wagonNode* wagon = NULL;
	int temporary_weight;
	while (weight > 0) 
	{
		wagon = GetNextAvailableWagon(material, head);		// Starting from head to check available wagoon.

		if (wagon == NULL)		// There is no suitable wagoon. Create new one.
		{
			wagon = new wagonNode();
			wagonNode* temp = head;
			while(temp != NULL)		// Make wagoon connected.
			{
				if (temp->next == NULL) 
				{
					temp->next = wagon;
					wagon->wagonId = temp->wagonId + 1;
					wagon->next = NULL;
					break;
				}
				temp = temp->next;
			}
		}
		materialNode* node = GetMaterialNode(material, wagon->material);


		if (node != NULL && node->weight + weight > MATERIAL_CAPACITY)
		{
			temporary_weight = MATERIAL_CAPACITY - node->weight;
		}
		else if (weight > MATERIAL_CAPACITY)					// If weight is bigger than 2000 load 2000 and continue next wagoon.
		{
			temporary_weight = MATERIAL_CAPACITY;
		}
		else 
		{
			temporary_weight = weight;
		}

		weight -= temporary_weight;			// Reduce the weight.

		if (node == NULL) 
		{
			node = new materialNode();
			node->id = material;
			node->next = NULL;
			node->weight = temporary_weight;
			wagon->materialNodeCount += 1;

			materialNode* temp = wagon->material;
			while (temp != NULL)		// Make material node connected.
			{
				if (temp->next == NULL)
				{
					temp->next = node;
					node->next = NULL;
					break;
				}
				temp = temp->next;
			}
		}
		else 
		{
			//node->id = material;
			node->weight += temporary_weight;
			//node->next = NULL;
		}

		if (wagon->material == NULL) {
			wagon->material = node;
		}
		else 
		{
			//materialNode* temp = wagon->material;
			//while (temp != NULL)		// Make material node connected.
			//{
			//	if (temp->next == NULL)
			//	{
			//		temp->next = node;
			//		node->next = NULL;
			//		break;
			//	}
			//	temp = temp->next;
			//}
		}
		reorderMaterialNodes(wagon->material, wagon->materialNodeCount);		// Reorder the material nodes.
	}
};

void Train::reorderMaterialNodes(materialNode* head_node, int size) 
{
	materialNode* first_node  = head_node;
	materialNode* second_node = head_node;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size - 1; j++)
		{
			if (first_node->id < second_node->id)
			{
				char temp			= first_node->id;                  // This part changes the id of two nodes.
				int weight_temp     = first_node->weight;                  // This part changes the weigth of two nodes.
				first_node->id	    = second_node->id;
				first_node->weight  = second_node->weight;
				second_node->id     = temp;
				second_node->weight = weight_temp;
			}
			second_node = second_node->next;        // Iterate the another node.
		}
		second_node = head_node;
		first_node = first_node->next;
	}
}

void Train::deleteFromWagon(char material, int weight) 
{
    //FILL THIS FUNCTION ACCORDINGLY
	while (weight != 0)
	{
		wagonNode* last_wagon = GetLastWagon();		// To start from las wagon.

		wagonNode* available_wagon  = GetNextAvailableWagonUnloading(material, last_wagon);
		materialNode* material_node = GetMaterialNodeUnloading(material, available_wagon->material);

		if (material_node->weight - weight < 0) 
		{
			weight -= material_node->weight;
			material_node->weight = 0;
		}
		else if (material_node->weight - weight >= 0)
		{
			material_node->weight -= weight;
			weight = 0;
		}

		if (material_node->weight == 0)
		{
			if (material_node->next == NULL && GetFormerNode(available_wagon, material_node) == NULL)	// Only one node
			{
				//materialNode* temp = GetFormerNode(available_wagon, material_node);	// Make the material of the wagon null.
			}
			else if(material_node->next != NULL && GetFormerNode(available_wagon, material_node) != NULL)	// Middle node
			{
				materialNode* temp = GetFormerNode(available_wagon, material_node);
				temp->next = material_node->next;
			}
			else if (material_node->next != NULL && GetFormerNode(available_wagon, material_node) == NULL)		// First node
			{
				available_wagon->material = material_node->next;
			}
			else if (material_node->next == NULL && GetFormerNode(available_wagon, material_node) != NULL)		// First node
			{
				materialNode* temp = GetFormerNode(available_wagon, material_node);
				temp->next = NULL;
			}
			available_wagon->materialNodeCount -= 1;
		}

		while (true)
		{
			bool founded = false;
			if (true == wagonIsEmpty(last_wagon))
			{
				wagonNode* lastwagonisempty = GetFormerWagon(last_wagon);
				last_wagon = GetFormerWagon(last_wagon);
				founded = true;
				lastwagonisempty->next = NULL;
			}
			if (last_wagon == head) break;

			if (founded == false) {
				last_wagon = GetFormerWagon(last_wagon);
			}
		}

		if (available_wagon == head) break;		// If there is no any option for unloading break the loop.
	}
};

wagonNode* Train::GetNextAvailableWagonUnloading(char material, wagonNode* last_wagon) 
{
	wagonNode* current = last_wagon;
	materialNode* material_node = GetMaterialNode(material, current->material);
	if (material_node == NULL) 
	{
		while (true)
		{
			current = GetFormerWagon(current);
			material_node = GetMaterialNode(material, current->material);
			if (material_node != NULL && material_node->weight != 0) {
				return current;
			}
			if (current == head) break;
		}
	}
	return current;
}


wagonNode* Train::GetLastWagon() 
{
	wagonNode* temporary_wagon = head;
	while (temporary_wagon != NULL)
	{
		if (temporary_wagon->next == NULL) {
			return temporary_wagon;
		}
		temporary_wagon = temporary_wagon->next;
	}
	return temporary_wagon;
}

wagonNode* Train::GetFormerWagon(wagonNode* last_wagon)
{
	wagonNode* temporary_wagon = last_wagon;
	wagonNode* head_wagon	   = head;
	if (temporary_wagon == head_wagon) return temporary_wagon;

	while (head_wagon != NULL)
	{
		if (head_wagon->next == temporary_wagon) {
			return head_wagon;
		}
		head_wagon = head_wagon->next;
	}
	return head_wagon;
}

materialNode* Train::GetFormerNode(wagonNode* wagon, materialNode* last_node)
{
	materialNode* temporary_node  = last_node;
	materialNode* head_node	      = wagon->material;
	if (head_node == temporary_node) 
	{
		wagon->material = NULL;
		return NULL;
	} 
	while (head_node != NULL)
	{
		if (head_node->next == temporary_node) {
			return head_node;
		}
		head_node = head_node->next;
	}
	return head_node;
}

bool Train::wagonIsEmpty(wagonNode* current_wagon) 
{
	materialNode* temp = current_wagon->material;

	if (temp != NULL)
	{
		while (temp != NULL)
		{
			if (temp->weight != 0) 
			{
				return false;
			}
			temp = temp->next;
		}
	}
	return true;
}



void Train::printWagon() {
    wagonNode* tempWagon = head;

    if (tempWagon == NULL) {
        cout << "Train is empty!!!" << endl;
        return;
    }

    while (tempWagon != NULL) {
        materialNode* tempMat = tempWagon->material;
        cout << tempWagon->wagonId << ". Wagon:" << endl;
        while (tempMat != NULL) {
            cout << tempMat->id << ": " << tempMat->weight << "KG, ";
            tempMat = tempMat->next;
        }
        cout << endl;
        tempWagon = tempWagon->next;
    }
    cout << endl;
};