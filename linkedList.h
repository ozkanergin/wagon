#ifndef _H
#define _H

#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

class materialNode {
public:
    char id;
    int weight;
    materialNode* next;
};

class wagonNode {
public:
    int wagonId;
    materialNode* material;
    wagonNode* next;
	int materialNodeCount;
};

class Train {

    wagonNode* head;

public:
    void create();
    void addMaterial(char, int);
    void deleteFromWagon(char, int);
    void printWagon(); //Prints wagon info
    materialNode* GetMaterialNode(char, materialNode*);
    materialNode* GetMaterialNodeUnloading(char, materialNode*);
    bool IsMaterialExists(char, materialNode*);
    wagonNode* GetNextAvailableWagon(char, wagonNode*);
    wagonNode* GetNextAvailableWagonUnloading(char, wagonNode*);
    wagonNode* GetLastWagon();
    wagonNode* GetFormerWagon(wagonNode*);
	materialNode* GetFormerNode(wagonNode* ,materialNode*);
	void reorderMaterialNodes(materialNode*, int);
	bool wagonIsEmpty(wagonNode*);

};

#endif